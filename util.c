/*
 * util.c
 *
 * Created: 04.01.2020 14:30:06
 *  Author: Peszi
 */ 

#include <avr/io.h>
#include "util.h"

uint8_t btnChange = 0;
uint8_t btnState = 0;
uint8_t btnLastState = 0;

void UTIL_init_IO(void) {
	// ext
	DDRD |= OUT_FAN;
	DDRD |= OUT_BUZZER;
	// btns
	DDRD &= ~0xE0;
	DDRB &= ~0x01;
	PORTD |= 0xE0;
	PORTB |= 0x01;	
}

void UTIL_update_IO(void) {
	btnState = UTIL_get_btns_state();
	btnChange = (btnState != btnLastState);
}

void UTIL_end_IO(void) {
	btnLastState = btnState;
}

void UTIL_set_fan(uint8_t state) {
	if (state) { PORTD |= OUT_FAN; } else { PORTD &= ~OUT_FAN; };	
}

char UTIL_get_btns_state(void) {
	register char state = 0;
	if (BTN_OK_PRESSED) { state |= (1 << BTN_OK); }
	if (BTN_UP_PRESSED) { state |= (1 << BTN_UP); }
	if (BTN_DOWN_PRESSED) { state |= (1 << BTN_DOWN); }
	if (BTN_ESC_PRESSED) { state |= (1 << BTN_ESC); }
	return state;
}

char UTIL_is_btn_pressed(const char id) {
	if (!btnChange) return 0;
	const char btnMask = (1 << id);
	return (btnState & btnMask);
}

char UTIL_inc_dec_val(unsigned char* value, unsigned char limit, char inc) {
	if (inc) {
		if (*value < (limit - 1))  { *value += 1; } else { return 0; }
	} else {
		if (*value > 0)  { *value -= 1; } else { return 0; }
	}
	return 1;
}

uint8_t UTIL_inc_dec_val_word(uint16_t* value, uint16_t limit, uint16_t inc) {
	if (inc) {
		if (*value < (limit - 1))  { *value += 1; } else { return 0; }
	} else {
		if (*value > 0)  { *value -= 1; } else { return 0; }
	}
	return 1;
}