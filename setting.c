/*
 * setting.c
 *
 * Created: 04.01.2020 14:42:54
 *  Author: Peszi
 */ 

#include <avr/eeprom.h>
#include "setting.h"
#include "util.h"
#include "display.h"
#include "include/hd44780.h"

uint8_t EEMEM alarmWarnEEP;
uint8_t EEMEM alarmDangerEEP;
uint16_t EEMEM alarmMuteEEP;

uint8_t menuSelectionIdx = 0;
uint8_t menuTimerMin = 0;

uint8_t alarmAnyTrigger = 0;

uint8_t alarmMute = 0;
uint16_t alarmMuteTime = 0;

uint16_t alarmMuteValue = 0;
uint16_t alarmMuteSetting = 0;

uint8_t alarmShortTimer = 0;

void SETTING_alarm_setup_eeprom(struct AlarmSetting* alarm, uint8_t* eepromPtr) {
	uint8_t eepromVal = eeprom_read_byte(eepromPtr);
	if (eepromVal == 0xff) {
		(*alarm).setting = SETTING_DEFAULT_ALARM_TEMP_VALUE; // some value
		(*alarm).value = (*alarm).setting;
		eeprom_write_byte(eepromPtr, (*alarm).setting);
	} else {
		(*alarm).setting = eepromVal;
		(*alarm).value = (*alarm).setting;
	}
}

void SETTING_alarm_load_data(void) {
	SETTING_alarm_setup_eeprom(&warningAlarm, &alarmWarnEEP);
	SETTING_alarm_setup_eeprom(&dangerAlarm, &alarmDangerEEP);
	// mute
	
	uint16_t eepromVal = eeprom_read_word(&alarmMuteEEP);
	if (eepromVal == 0xffff) {
		alarmMuteSetting = SETTING_DEFAULT_ALARM_MUTE_VALUE; // some value
		alarmMuteValue = alarmMuteSetting;
		eeprom_write_word(&alarmMuteEEP, alarmMuteSetting);
	} else {
		alarmMuteSetting = eepromVal;
		alarmMuteValue = alarmMuteSetting;
	}	
}

void SETTING_update_menu(void) {
	lcd_clrscr();
	// Header
	lcd_goto(0x00); lcd_puts(" ---- Menu ---- ");
	// Menu setting selection
	if (UTIL_is_btn_pressed(BTN_UP))
		UTIL_inc_dec_val(&menuSelectionIdx, 3, 1);
	if (UTIL_is_btn_pressed(BTN_DOWN)) 
		UTIL_inc_dec_val(&menuSelectionIdx, 0, 0);
	// Display selection
	lcd_goto(0x40);
	switch (menuSelectionIdx) {
		case 0: lcd_puts("|    Alarm I   >"); break;	
		case 1: lcd_puts("<   Alarm II   >"); break;
		case 2: lcd_puts("<  Alarm Mute  |"); break;		
	}
}

void SETTING_alarm_change_data(char* name, struct AlarmSetting* alarm, uint8_t* eepromPtr) {
	// Header
	lcd_goto(0x00); lcd_puts(name);
	// Actual alarm value
	if (UTIL_is_btn_pressed(BTN_UP))
		UTIL_inc_dec_val(&(*alarm).setting, 120, 1);
	if (UTIL_is_btn_pressed(BTN_DOWN)) 
		UTIL_inc_dec_val(&(*alarm).setting, 0, 0);
	LCD_write_digit(0x41, (*alarm).setting);
	lcd_puts("oC");
	// Store alarm setting
	if (UTIL_is_btn_pressed(BTN_OK)) {
		eeprom_write_byte(eepromPtr, (*alarm).setting);
		(*alarm).value = (*alarm).setting;
	}		
	// Store state info
	if ((*alarm).value == (*alarm).setting) {
		lcd_goto(0x4a); lcd_puts("(+/-)");
	} else {
		lcd_goto(0x4b); lcd_puts("(OK)");	
	}
}

void SETTING_alarm_mute_change(void) {
	// Header
	lcd_goto(0x00); lcd_puts(" - Alarm Mute - ");
	// Change mute value
	if (UTIL_is_btn_pressed(BTN_UP))
		UTIL_inc_dec_val_word(&alarmMuteSetting, SETTING_ALARM_MAX_MUTE, 1);
	if (UTIL_is_btn_pressed(BTN_DOWN)) 
		UTIL_inc_dec_val_word(&alarmMuteSetting, 0, 0);
	LCD_write_digit(0x41, alarmMuteSetting);
	lcd_puts("min");
	
	// Store alarm mute
	if (UTIL_is_btn_pressed(BTN_OK)) {
		eeprom_write_word(&alarmMuteEEP, alarmMuteSetting);
		alarmMuteValue = alarmMuteSetting;
	}		
	// Store state info
	if (alarmMuteValue == alarmMuteSetting) {
		lcd_goto(0x4a); lcd_puts("(+/-)");
	} else {
		lcd_goto(0x4b); lcd_puts("(OK)");	
	}
}

void SETTING_update_change(void) {
	lcd_clrscr();
	switch (menuSelectionIdx) {
		case 0: SETTING_alarm_change_data(" -- Alarm  I -- ", &warningAlarm, &alarmWarnEEP); break;	
		case 1: SETTING_alarm_change_data(" -- Alarm II -- ", &dangerAlarm, &alarmDangerEEP); break;
		case 2: SETTING_alarm_mute_change(); break;
	}
}

void SETTING_update_timers_min(void) {
	if (alarmMute) {
		if (alarmMuteTime > 0) {
			alarmMuteTime--;
		} else {
			alarmMute = 0;	
		}
	}
}

void SETTING_update_timers(void) {
	
	if (menuTimerMin >= 60) {
		menuTimerMin = 0;
		SETTING_update_timers_min();
	}
	menuTimerMin++;
	
	if (alarmMute || !alarmAnyTrigger) {
		PORTD &= ~OUT_BUZZER;
	} else if (alarmAnyTrigger) {
		if (dangerAlarm.trigger) {
			PORTD |= OUT_BUZZER;
		} else {
			if (alarmShortTimer < SETTING_WARN_ALARM_SEC_ON) {
				PORTD ^= OUT_BUZZER;
			} else if (alarmShortTimer >= SETTING_WARN_ALARM_SEC_OFF) {
				alarmShortTimer = 0;
			} else {
				PORTD &= ~OUT_BUZZER;
			}
			alarmShortTimer++;
		}
	}
}