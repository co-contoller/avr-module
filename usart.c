/*
 * usart.c
 *
 * Created: 04.04.2019 20:39:44
 *  Author: Peszi
 */ 

#include <avr/io.h>
#include <string.h>
#include "usart.h"

void USART_init(void) {
	UBRRH = (USART_BAUD_PRESCALE >> 8);
	UBRRL = USART_BAUD_PRESCALE;
	
	UCSRB |= (1 << RXEN)|(1 << TXEN);
	UCSRC |= (1 << UCSZ0)|(1 << UCSZ1);//|(1 << UMSEL00);	// 8bit
}

void USART_flush(const char *buffer) {
	const uint8_t buffer_str_len = strlen(buffer);
	uint8_t buffer_idx = 0;
	while (buffer_idx < buffer_str_len) {
		while(!(UCSRA & (1 << UDRE)));
		UDR = buffer[buffer_idx++];
	}
	//memset(buffer, 0, buffer_str_len);
}

uint8_t USART_listen(char *buffer) {
	const uint8_t buffer_size = sizeof(buffer);
	uint8_t buffer_idx = 0;
	uint32_t tick_count = 0;
	uint8_t data = 0;
	while (tick_count < F_CPU) {
		if (UCSRA & (1 << RXC)) {
			data = UDR;
			if (data == '\n' || buffer_idx + 1 >= buffer_size) {
				break;
			}
			buffer[buffer_idx++] = data;
		}
		tick_count++;
	}
	return buffer_idx;
}
