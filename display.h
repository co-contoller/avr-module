/*
 * display.h
 *
 * Created: 04.01.2020 14:30:17
 *  Author: Peszi
 */ 

#ifndef DISPLAY_H_
	#define DISPLAY_H_
#endif /* DISPLAY_H_ */

#define CHAR_BYTES_LEN 32
#define CHAR_NUMBERS_LEN 10

void LCD_write_digit(uint8_t position, unsigned char value);

void LCD_write_big_digit(unsigned int location, unsigned int value);

void LCD_write_big_digits(unsigned int offset, unsigned int value);
