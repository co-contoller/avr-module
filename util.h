/*
 * util.h
 *
 * Created: 04.01.2020 14:30:17
 *  Author: Peszi
 */

#ifndef UTIL_H_
	#define UTIL_H_
	extern uint8_t btnChange;
	extern uint8_t btnState;
#endif /* UTIL_H_ */

#define OUT_BUZZER	(1 << 2)
#define OUT_FAN		(1 << 4)

#define BTN_OK_PRESSED		!(PIND & (1 << 5))
#define BTN_DOWN_PRESSED	!(PIND & (1 << 6))
#define BTN_UP_PRESSED		!(PIND & (1 << 7))
#define BTN_ESC_PRESSED		!(PINB & (1 << 0))

#define BTN_OK		0
#define BTN_DOWN	1
#define BTN_UP		2
#define BTN_ESC		3

void UTIL_init_IO(void);

void UTIL_update_IO(void);

void UTIL_end_IO(void);

void UTIL_set_fan(uint8_t state);

char UTIL_get_btns_state(void);

char UTIL_is_btn_pressed(const char id);

char UTIL_inc_dec_val(unsigned char* value, unsigned char limit, char inc);

uint8_t UTIL_inc_dec_val_word(uint16_t* value, uint16_t limit, uint16_t inc);